# Fedex Test Automation Project using Cucumber Selenium Java
This project is a frontend test automation framework in Selenium, Cucumber BDD using below versions.

Java version **14**
Selenium version: **3.141.59**
chrome version: **98.0.4758.109**

Application under test is FedEx browser application home page. Project has been created for FedEx, as test assignment.
WebElement with expected condition is used in project.

## Getting Started and commands to run test

This project is Maven Project , Maven commands can be used to build the project and run them.

To build and run all test use below mentioned command:

**mvn clean install**

To run feature specific scenarios. For example To run all test with tag `@Smoke_Test use below command:

**mvn clean test -Dcucumber.options="src/test/resources --tags @Smoke_Test"**

To run specific scenario. Use the tag mentioned on specific scenarios ex @ShippingServicesMenu

**mvn clean test -Dcucumber.options="src/test/resources --tags @@ShippingServicesMenu"**

## Below scenarios are covered in this project

- Validate Shipment tracking from main page of Fedex application : **ShipmentTrackingMain.feature**
- Validate Tracking from menu on Fedex application: **ShipmentTrackingMenu.feature**
- Validate Support menu on Fedex application: **SupportMenu.feature**
- Validate Shipping menu on Fedex application: **ShippingNavigationMenu.feature**
- Validate Social media follow links on Fedex application: **SocialFooterIcons.feature**