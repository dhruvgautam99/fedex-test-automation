package factory;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import static org.openqa.selenium.remote.CapabilityType.*;
import static org.openqa.selenium.remote.CapabilityType.ACCEPT_INSECURE_CERTS;

public class FirefoxDriverFactory implements Driver {
    private FirefoxOptions firefoxOptions;

    @Override
    public RemoteWebDriver getWebDriver() throws Exception {
        WebDriverManager.firefoxdriver().setup();
        try{
            return new FirefoxDriver(getFirefoxOptions());
        }
        catch (Exception e){
            throw new Exception ("Unable to instantiate FirefoxDriver", e);
        }
    }

    private FirefoxOptions getFirefoxOptions() {
        firefoxOptions = new FirefoxOptions();
        firefoxOptions.setHeadless(headless);
        firefoxOptions.addArguments("--window-size=3056, 2034");
        firefoxOptions.setCapability(SUPPORTS_ALERTS, false);
        firefoxOptions.setCapability(SUPPORTS_LOCATION_CONTEXT, true);
        firefoxOptions.setCapability(SUPPORTS_JAVASCRIPT, true);
        firefoxOptions.setCapability(ACCEPT_INSECURE_CERTS, true);
        firefoxOptions.addArguments("enable-automation");
        firefoxOptions.addArguments("--no-sandbox");
        firefoxOptions.addArguments("--allow-running-insecure-content");
        firefoxOptions.addArguments("--ignore-certificate-errors");
        firefoxOptions.addArguments("--ignore-ssl-errors");
        return firefoxOptions;
    }
}
