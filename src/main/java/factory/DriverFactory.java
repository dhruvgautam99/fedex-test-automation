package factory;

import configuration.ConfigurationManager;
import org.openqa.selenium.remote.RemoteWebDriver;

public class DriverFactory {

    private static RemoteWebDriver webDriver;

    public static RemoteWebDriver getWebDriver(){
        return webDriver;
    }

    public static void setupDriver() throws Exception {
        String browser = ConfigurationManager.getConfiguration().browser();
        if(webDriver == null){
            switch (browser){
                case "chrome" : ChromeDriverFactory chromeDriverFactory = new ChromeDriverFactory();
                    webDriver = chromeDriverFactory.getWebDriver();
                    break;
                case "firefox" : FirefoxDriverFactory firefoxDriverFactory = new FirefoxDriverFactory();
                    webDriver = firefoxDriverFactory.getWebDriver();
                    break;
            }
        }
    }
    public static void closeDriver(){
        if(webDriver != null)
        {
            webDriver.quit();
        }
        webDriver = null;
    }
}
