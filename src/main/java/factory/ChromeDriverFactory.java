package factory;


import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.remote.RemoteWebDriver;

import static org.openqa.selenium.remote.CapabilityType.*;


public class ChromeDriverFactory implements Driver {

    private static ChromeOptions chromeOptions;

    @Override
    public RemoteWebDriver getWebDriver() throws Exception {
        WebDriverManager.chromedriver().setup();
        try{
            return new ChromeDriver(getChromeOptions());
        }
        catch (Exception e){
            throw new Exception ("Unable to instantiate ChromeDriver", e);
        }
    }

    private ChromeOptions getChromeOptions() {
        chromeOptions = new ChromeOptions();
        chromeOptions.setHeadless(headless);
        chromeOptions.addArguments("--window-size=1920,1080");
        chromeOptions.setCapability(SUPPORTS_ALERTS, false);
        chromeOptions.setCapability(SUPPORTS_LOCATION_CONTEXT, true);
        chromeOptions.setCapability(SUPPORTS_JAVASCRIPT, true);
        chromeOptions.setCapability(ACCEPT_INSECURE_CERTS, true);
        chromeOptions.addArguments("--allow-running-insecure-content");
        chromeOptions.addArguments("--ignore-certificate-errors");
        chromeOptions.addArguments("--ignore-ssl-errors");
        return chromeOptions;
    }
}
