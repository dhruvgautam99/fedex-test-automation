package factory;

import org.openqa.selenium.remote.RemoteWebDriver;
import configuration.ConfigurationManager;

public interface Driver {
    boolean headless = Boolean.parseBoolean(ConfigurationManager.getConfiguration().headless());
    RemoteWebDriver getWebDriver() throws Exception;
}
