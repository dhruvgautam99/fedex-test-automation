package configuration;

import org.aeonbits.owner.Config;

@Config.LoadPolicy(Config.LoadType.MERGE)
@Config.Sources({
        "system:properties",
        "system:env",
        "file:src/main/resources/environments/env.properties"
})
public interface Configuration extends Config {

    @Key("headless")
    @DefaultValue("false")
    String headless();

    @Key("browser")
    @DefaultValue("chrome")
    String browser();

    @Key("env.url")
    String environmentURL();
}
