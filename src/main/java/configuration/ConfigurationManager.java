package configuration;
import org.aeonbits.owner.ConfigFactory;

public abstract class ConfigurationManager {

    private static Configuration configuration;

    public static Configuration getConfiguration(){
        if(configuration == null)
            loadConfig();
        return configuration;
    }

    private static void loadConfig(){
        try{
            configuration = ConfigFactory.create(Configuration.class);
        }
        catch (NullPointerException nullPointerException){}
    }
}
