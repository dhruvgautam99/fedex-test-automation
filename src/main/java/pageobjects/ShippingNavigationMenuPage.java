package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShippingNavigationMenuPage extends BasePage{

    @FindBy(how= How.CSS, using="div[class='dropdown section'] a[data-analytics='hdr|tab|1||Shipping']")
    private WebElement clickOnShippingDropdownSelector;

    @FindBy(how= How.CSS, using="div[class='link section'] a[aria-label='ALL SHIPPING SERVICES']")
    private WebElement clickOnAllShippingServicesSelector;

    @FindBy(how= How.CSS, using="div[class='fxg-wrapper'] a[data-analytics='tab|Domestic']")
    private WebElement seesDomesticServicesSelector;


    @FindBy(how= How.CSS, using="div[class='fxg-wrapper'] a[data-analytics='tab|Europe and Worldwide']")
    private WebElement clickOnEuropeAndWorldwideServicesSelector;

    @FindBy(how= How.CSS, using="div[class='fxg-align-left'] a[data-analytics='hero|Ship Now']")
    private WebElement clicksOnShipNowSelectorButton;

    @FindBy(how= How.CSS, using="input[title='User ID']")
    private WebElement seesUserIdInputSelector;


    public void userClicksShippingDropdown() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(clickOnShippingDropdownSelector));
        clickOnShippingDropdownSelector.click();

    }

    public void userClicksAllShippingServices() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(clickOnAllShippingServicesSelector));
        clickOnAllShippingServicesSelector.click();
    }

    public void userSeesDomesticServices() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(seesDomesticServicesSelector));
        seesDomesticServicesSelector.isDisplayed();
        seesDomesticServicesSelector.click();
    }

    public void userClicksOnEuropeAndWorldwideServices() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(clickOnEuropeAndWorldwideServicesSelector));
        clickOnEuropeAndWorldwideServicesSelector.click();
    }

    public void userClicksOnShipNowButton() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(clicksOnShipNowSelectorButton));
        clicksOnShipNowSelectorButton.click();
    }

    public void userSeesUserIdAField() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(seesUserIdInputSelector));
        seesUserIdInputSelector.isDisplayed();
    }
}
