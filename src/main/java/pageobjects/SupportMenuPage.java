package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SupportMenuPage extends BasePage{

    @FindBy(how= How.CSS, using="a[aria-label='Support']")
    private WebElement userClicksSupportDropdown;

    @FindBy(how= How.CSS, using="a[aria-label='New Customer Centre']")
    private WebElement getNewCustomerCentreSelector;

    @FindBy(how= How.CSS, using="a[title='Customs Clearance']")
    private WebElement getCustomsClearanceSelector;

    @FindBy(how= How.CSS, using="a[aria-label='Claims']")
    private WebElement getClaimsSelector;

    @FindBy(how= How.CSS, using="a[aria-label='FAQs']")
    private WebElement getFAQsSelector;

    @FindBy(how= How.CSS, using="a[aria-label='CONTACT US']")
    private WebElement userClicksContactUsSelector;

    @FindBy(how= How.CSS, using=".nw_UserInputField")
    private WebElement userEnterMessageForSupport;

    @FindBy(how= How.CSS, using=".nw_UserSubmit")
    private WebElement userSubmitMessageForSupport;

    public void userClicksOnSupportDropdownButton() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(userClicksSupportDropdown));
        userClicksSupportDropdown.click();
    }

    public WebElement getNewCustomerCenterSelector() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(getNewCustomerCentreSelector));
        return getNewCustomerCentreSelector;
    }
    public WebElement getCustomsClearanceSelector() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(getCustomsClearanceSelector));
        return getCustomsClearanceSelector;
    }

    public WebElement getClaimsSelector() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(getClaimsSelector));
       return getClaimsSelector;
    }
    public WebElement getFAQSelector() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(getFAQsSelector));
        return getFAQsSelector;
    }

    public void userClicksOnContactUsButton() {
        WebDriverWait wait = new WebDriverWait (driver, timeoutInSeconds);
        wait.until (ExpectedConditions.visibilityOf (userClicksContactUsSelector));
        userClicksContactUsSelector.click ();
    }

    public void userEnterMessageForSupport(String message) {
        WebDriverWait wait = new WebDriverWait (driver, timeoutInSeconds);
        wait.until (ExpectedConditions.visibilityOf (userEnterMessageForSupport));
        userEnterMessageForSupport.sendKeys (message);
    }

    public void userSubmitMessageForSupport() {
        WebDriverWait wait = new WebDriverWait (driver, timeoutInSeconds);
        wait.until (ExpectedConditions.visibilityOf (userSubmitMessageForSupport));
        userSubmitMessageForSupport.click ();
    }
}
