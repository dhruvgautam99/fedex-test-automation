package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;
import java.util.Random;

public class ShipmentTrackingMainPage extends BasePage {

    @FindBy(how = How.CSS, using = "input[name='trackingnumber']")
    private WebElement insertTrackingIdNumberSelector;

    @FindBy(how = How.CSS, using = "#btnSingleTrack")
    private WebElement clickTrackButtonSelector;

    @FindBy(how= How.CSS, using="span[class='systemErrorMessageTop']")
    private WebElement trackingInvalidIdNoResultErrorMessage;

    @FindBy(how = How.CSS, using = "[title = 'Multiple Tracking Numbers']")
    private WebElement multipleTrackingEntriesSelectorButton;

    @FindBy(how = How.CSS, using = "#HomeMultiTrackingApp > div > div > input")
    private List<WebElement> multipleTrackingIdFields;

    @FindBy(how = How.CSS, using = "#btnMultiTrack")
    private WebElement clickMultipleTrackButtonSelector;

    public void userEntersInvalidTrackingNumberInTrackingIdField ( String trackingId ) {
        WebDriverWait wait = new WebDriverWait (driver, timeoutInSeconds);
        wait.until (ExpectedConditions.visibilityOf (insertTrackingIdNumberSelector));
        insertTrackingIdNumberSelector.sendKeys (trackingId);
    }

    public void userClicksTrackSingleShipmentButton () {
        WebDriverWait wait = new WebDriverWait (driver, timeoutInSeconds);
        wait.until (ExpectedConditions.visibilityOf (clickTrackButtonSelector));
        clickTrackButtonSelector.click ();
    }

    public void userClickMultipleTrackingEntriesButton () {
        WebDriverWait wait = new WebDriverWait (driver, timeoutInSeconds);
        wait.until (ExpectedConditions.visibilityOf (multipleTrackingEntriesSelectorButton));
        multipleTrackingEntriesSelectorButton.click ();
    }

    public void multipleTrackingFields () {
        WebDriverWait wait = new WebDriverWait (driver, timeoutInSeconds);
        wait.until (ExpectedConditions.visibilityOf (multipleTrackingIdFields.get (0)));
        for(int i=0; i<multipleTrackingIdFields.size (); i++)
        {
            multipleTrackingIdFields.get(i).sendKeys (""+new Random ().nextInt(100000 - 1000 + 1) + 1000);
        }
    }

    public void userClickMultipleEntriesTrackButton () {
        WebDriverWait wait = new WebDriverWait (driver, timeoutInSeconds);
        wait.until (ExpectedConditions.visibilityOf (clickMultipleTrackButtonSelector));
        clickMultipleTrackButtonSelector.click ();
    }
}