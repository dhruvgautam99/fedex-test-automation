package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class ShipmentTrackingMenuPage extends BasePage {

    @FindBy(how= How.CSS, using="a[aria-label='Tracking'] span[class='fxg-mouse']")
    private WebElement clickOnTrackingDropdownSelector;

    @FindBy(how= How.ID, using="trackingModuleTrackingNum")
    private WebElement insertTrackingIdNumberSelector;

    @FindBy(how= How.CSS, using="button[aria-label='Click here to track your package']")
    private WebElement clickOnTrackShipmentSelector;

    @FindBy(how= How.CSS, using="span[id*='fxg-validation-']")
    private WebElement trackingIdErrorMessage;

    @FindBy(how= How.CSS, using="a[aria-label='Customised FedEx Tracking']")
    private WebElement customisedTrackingSelector;

    @FindBy(how= How.CSS, using="a[aria-label='FedEx Insight']")
    private WebElement insightTrackingSelector;

    @FindBy(how= How.CSS, using="a[aria-label='Log In ']")
    private WebElement customisedTrackingLoginButton;

    @FindBy(how= How.ID, using="title")
    private WebElement loginPageTitle;

    public void userClicksTrackingDropdown() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(clickOnTrackingDropdownSelector));
        clickOnTrackingDropdownSelector.click();
    }

    public void userEntersNothingInTrackingId() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(clickOnTrackShipmentSelector));
        clickOnTrackShipmentSelector.sendKeys ("");
    }

    public void userEntersInvalidTrackingNumberInTrackingId(String trackingId) {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(insertTrackingIdNumberSelector));
        insertTrackingIdNumberSelector.sendKeys(trackingId);
    }

    public void userClickShipmentTrackButton() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(clickOnTrackShipmentSelector));
        clickOnTrackShipmentSelector.click ();
    }

    public String getTrackingIdErrorMessage(){
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(trackingIdErrorMessage));
        return trackingIdErrorMessage.getText ();
    }

    public WebElement getCustomisedTrackingSelector(){
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(customisedTrackingSelector));
        return customisedTrackingSelector;
    }

    public WebElement getInsightTrackingSelector(){
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(insightTrackingSelector));
        return insightTrackingSelector;
    }

    public void userClicksCustomisedTrackingLoginButton(){
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(customisedTrackingLoginButton));
         customisedTrackingLoginButton.click ();
    }

    public String getLoginPageTitleSelector(){
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(loginPageTitle));
        return loginPageTitle.getText ();
    }
}
