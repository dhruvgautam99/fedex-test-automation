package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SocialFooterIconsPage extends BasePage{

    @FindBy(how= How.CSS, using="img[class='fxg-icon fxg-icon--twitter   ']")
    private WebElement clickTwitterImageSelector;

    @FindBy(how= How.ID, using="title")
    private WebElement twitterPageTitle;

    public void userClicksOnTwitterImage() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(clickTwitterImageSelector));
        clickTwitterImageSelector.click();
    }

    public String getTwitterPageTitleSelector(){
        return driver.getCurrentUrl ();
    }
}
