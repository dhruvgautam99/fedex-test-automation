package pageobjects;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class CommonPageComponent extends BasePage {

    @FindBy(how = How.CSS, using = "div[class='notification__message']")
    public WebElement trackingInvalidIdSystemErrorMessage;

    public WebElement webElementAfterWaitForVisibility ( WebElement element ){
        WebDriverWait wait = new WebDriverWait ( driver, timeoutInSeconds );
        wait.until (ExpectedConditions.visibilityOf (element));
        return element;
    }
}
