package pageobjects;

import factory.DriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BasePage {
    protected static WebDriver driver;
    public static final int timeoutInSeconds = 20;

    @FindBy(how = How.CSS, using = "a[aria-label='English'][data-country-code='nl']")
    public WebElement nlLocation;

    @FindBy(how= How.CLASS_NAME, using="fxg-cookie-consent__actions")
    public WebElement acceptCookiePolicy;

    public BasePage () {
        driver= DriverFactory.getWebDriver ();
        PageFactory.initElements(driver, this);
    }

    public void clickNlLocation() {
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(nlLocation));
        nlLocation.click();
    }

    public void acceptCookiePolicy(){
        WebDriverWait wait = new WebDriverWait(driver, timeoutInSeconds);
        wait.until(ExpectedConditions.visibilityOf(acceptCookiePolicy));
        acceptCookiePolicy.click();
    }
}
