Feature: Navigate to home page of Fedex application and validate the Tracking from main page

  Background:
    Given I navigate to fedex application select english language and accept cookies

  @Smoke_Test
  Scenario: As a user verify Tracking of shipment from main page
    Given User input shipment tracking ID: "D123456"
    And User clicks on single shipment tracking button
    Then User sees no result for Tracking ID and error message is displayed

  @Smoke_Test
  Scenario: As a user verify Tracking of shipment from main page with multiple invalid Tracking Id
    Given User click on Multiple Tracking Numbers option
    Then I input invalid tracking IDs into all 3 tracking fields
    And User clicks on multiple shipment tracking button
    Then User sees no result for Tracking ID and error message is displayed
