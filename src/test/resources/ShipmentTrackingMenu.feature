Feature: Navigate to home page of Fedex application and validate the tracking id section

  Background:
    Given I navigate to fedex application select english language and accept cookies

  @Negative_Scenario @Smoke_Test
  Scenario: As a user search shipment from Tracking menu without any tracking id
    Given User clicks on Tracking section
    And User does not fill the Tracking id of shipment
    When User clicks on the TRACK button to track the shipment
   Then User receives warning to add at least enter one tracking number

  @Negative_Scenario @Smoke_Test
  Scenario: As a user search shipment with not existing tracking id
    Given User clicks on Tracking section
    And User fills the Tracking id "ABFVSFRST"
    When User clicks on the TRACK button to track the shipment
    Then User receives system error on entering invalid tracking number

  @Smoke_Test
  Scenario: As a user verify Tracking menu sub items
    Given User clicks on Tracking section
    And User verifies the items under Tracking menu

  @Smoke_Test
  Scenario: As a user navigate to Customised fedex Tracking from Tracking menu and verify the Login screen display on Log in
    Given User clicks on Tracking section
    And User clicks on Customised FedEx Tracking menu
    And User clicks on Login button in Customised FedEx Tracking
    Then User lands on page to Login

