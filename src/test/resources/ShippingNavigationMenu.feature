Feature: Navigate to home page of Fedex application and verify Shipping menu options

  Background:
    Given I navigate to fedex application select english language and accept cookies

  @ShippingServicesMenu @Smoke_Test
  Scenario: As a User I want to see All Shipping Services and Start Ship now
    Given User clicks on Shipping dropdown menu
    When User clicks on All shipping Services menu
    And User Sees and click on Domestic Services
    And User clicks on Europe and Worldwide Services
    Then User Clicks on Ship Now
    And User Sees User Id field to Enter