Feature: Navigate to home page of Fedex application and verify Social footer section icons

  Background:
    Given I navigate to fedex application select english language and accept cookies

  @Smoke_Test
  Scenario: Navigate to Fedex application and validate Social footer section
    Given User clicks on Twitter icon button from social footer section
    Then User is redirected to Fedex twitter page