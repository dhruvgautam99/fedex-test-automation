Feature: Navigate to home page of Fedex application and verify Support menu and Contact support

  Background:
    Given I navigate to fedex application select english language and accept cookies

  @ShippingServicesMenu @Smoke_Test
  Scenario: As a User I want to see All Support menu services
    Given User clicks on Support dropdown menu
    And User verifies New Customer Center sub menu
    And User verifies Custom Clearance sub menu
    And User verifies Claims sub menu
    And User verifies FAQs sub menu

  @ShippingServicesMenuContactUs @Smoke_Test
  Scenario: As a User I want to contact Support from Support menu services
    Given User clicks on Support dropdown menu
    And User clicks on Contact Us in Support sub menu
    And User enter message for Support "Hi Please help me with tracking the shipment"
    Then User submit message for Support
