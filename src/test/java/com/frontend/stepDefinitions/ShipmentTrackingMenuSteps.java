package com.frontend.stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.junit.Assert;
import pageobjects.CommonPageComponent;
import pageobjects.ShipmentTrackingMenuPage;

import static junit.framework.TestCase.assertEquals;

public class ShipmentTrackingMenuSteps {

     ShipmentTrackingMenuPage shipmentTrackingMenuPage = new ShipmentTrackingMenuPage ();
     CommonPageComponent commonPageComponent= new CommonPageComponent();

     @Given("User clicks on Tracking section$")
     public void userClicksOnShippingDropdown () {
          shipmentTrackingMenuPage.userClicksTrackingDropdown();
     }

     @And("User fills the Tracking id \"([^\"]*)\"$")
     public void userFillsTheTrackingId (String trackingId ) {
          shipmentTrackingMenuPage.userEntersInvalidTrackingNumberInTrackingId(trackingId);
     }

     @And("User does not fill the Tracking id of shipment")
     public void userDoesNotFillTheTrackingId () {
          shipmentTrackingMenuPage.userEntersNothingInTrackingId();
     }

     @When("User clicks on the TRACK button to track the shipment")
     public void userClicksOnTheTRACKButtonToTrackTheShipment () {
          shipmentTrackingMenuPage.userClickShipmentTrackButton();
     }

     @Then("User receives warning to add at least enter one tracking number")
     public void userReceivesWarningToAddAtLeastEnterOneTrackingNumber () {
         String errorMessage= shipmentTrackingMenuPage.getTrackingIdErrorMessage();
          Assert.assertEquals("Please enter at least one tracking number.", shipmentTrackingMenuPage.getTrackingIdErrorMessage());
     }

     @Then("User receives system error on entering invalid tracking number")
     public void userReceivesSystemErrorForInvalidTrackingNumber () {
          commonPageComponent.webElementAfterWaitForVisibility(commonPageComponent.trackingInvalidIdSystemErrorMessage);
          Assert.assertTrue (commonPageComponent.trackingInvalidIdSystemErrorMessage.isDisplayed ());
     }


     @And("User verifies the items under Tracking menu")
     public void userVerifiesTheItemsUnderTrackingMenu () {
          Assert.assertTrue (shipmentTrackingMenuPage.getCustomisedTrackingSelector ().isDisplayed ());
          Assert.assertTrue (shipmentTrackingMenuPage.getInsightTrackingSelector ().isDisplayed ());
     }

     @And("User clicks on Customised FedEx Tracking menu")
     public void userClicksOnCustomisedFedExTrackingMenu () {
          shipmentTrackingMenuPage.getCustomisedTrackingSelector ().click ();
     }

     @And("User clicks on Login button in Customised FedEx Tracking")
     public void userClicksOnLoginButtonInCustomisedFedExTracking () {
       shipmentTrackingMenuPage.userClicksCustomisedTrackingLoginButton ();
     }

     @Then("User lands on page to Login")
     public void userLandsOnPageToLogin () {
          shipmentTrackingMenuPage.getLoginPageTitleSelector();
          assertEquals("Enter your user ID and password to log in",  shipmentTrackingMenuPage.getLoginPageTitleSelector());
     }
}