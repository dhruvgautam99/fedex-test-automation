package com.frontend.stepDefinitions;

import configuration.ConfigurationManager;
import cucumber.api.java.en.Given;
import factory.DriverFactory;
import lombok.extern.slf4j.Slf4j;
import org.openqa.selenium.WebDriver;
import pageobjects.BasePage;

@Slf4j
public class BackgroundSteps {
    WebDriver driver   =  DriverFactory.getWebDriver();
    BasePage basePage = new BasePage ();

    @Given("^I navigate to fedex application select english language and accept cookies$")
    public void iNavigateToFedexWebsiteSelectLanguageAndAcceptCookiePolicy() {
        driver.get (ConfigurationManager.getConfiguration ().environmentURL());
        log.info ("Select location , language and accept cookies.");
        basePage.clickNlLocation ();
        basePage.acceptCookiePolicy ();
    }
}
