package com.frontend.stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pageobjects.ShippingNavigationMenuPage;

public class ShippingNavigationMenuSteps {
    ShippingNavigationMenuPage shippingNavigationMenuPage = new ShippingNavigationMenuPage ();

    @Given("User clicks on Shipping dropdown menu")
    public void userClicksOnShippingDropdown () {
        shippingNavigationMenuPage.userClicksShippingDropdown();
    }

    @When("User clicks on All shipping Services menu")
    public void userClicksOnAllShippingServices () {
        shippingNavigationMenuPage.userClicksAllShippingServices();
    }

    @And("User Sees and click on Domestic Services")
    public void userSeesDomesticServices () {
        shippingNavigationMenuPage.userSeesDomesticServices ();
    }

    @And("User clicks on Europe and Worldwide Services")
    public void userClicksOnEuropeAndWorldwideServices () {
        shippingNavigationMenuPage.userClicksOnEuropeAndWorldwideServices();
    }

    @Then("User Clicks on Ship Now")
    public void userClicksOnShipNow () {
        shippingNavigationMenuPage.userClicksOnShipNowButton();
    }

    @And("^User Sees User Id field to Enter$")
    public void userSeesUserIdFields() {
        shippingNavigationMenuPage.userSeesUserIdAField();
    }
}
