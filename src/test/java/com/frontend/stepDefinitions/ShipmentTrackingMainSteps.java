package com.frontend.stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import pageobjects.CommonPageComponent;
import pageobjects.ShipmentTrackingMainPage;

public class ShipmentTrackingMainSteps {

    ShipmentTrackingMainPage shipmentTrackingMainPage= new ShipmentTrackingMainPage();
    CommonPageComponent commonPageComponent= new CommonPageComponent();

    @Given("User input shipment tracking ID: \"([^\"]*)\"$")
    public void userInputShipmentTrackingID ( String trackingId ) {
        shipmentTrackingMainPage.userEntersInvalidTrackingNumberInTrackingIdField(trackingId);
    }

    @And("User clicks on single shipment tracking button")
    public void userClicksOnSingleShipmentTrackingButton () {
        shipmentTrackingMainPage.userClicksTrackSingleShipmentButton();
    }

    @Then("User sees no result for Tracking ID and error message is displayed")
    public void userSeesNoResultForTrackingIDAndErrorMessageIsDisplayed () {
      commonPageComponent.webElementAfterWaitForVisibility(commonPageComponent.trackingInvalidIdSystemErrorMessage);
      Assert.assertTrue (commonPageComponent.trackingInvalidIdSystemErrorMessage.isDisplayed ());
    }

    @Given("User click on Multiple Tracking Numbers option")
    public void userClickOnMultipleTrackingNumbersOption () {
        shipmentTrackingMainPage.userClickMultipleTrackingEntriesButton();
    }

    @Then("I input invalid tracking IDs into all 3 tracking fields")
    public void iInputInvalidTrackingIDsIntoAllTrackingFields () {
        shipmentTrackingMainPage.multipleTrackingFields ();
    }

    @And("User clicks on multiple shipment tracking button")
    public void userClicksOnMultipleShipmentTrackingButton () {
        shipmentTrackingMainPage.userClickMultipleEntriesTrackButton();
    }
}
