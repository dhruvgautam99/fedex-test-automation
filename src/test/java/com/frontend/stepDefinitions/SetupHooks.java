package com.frontend.stepDefinitions;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import factory.DriverFactory;
import org.openqa.selenium.WebDriver;

public class SetupHooks {

    private static WebDriver driver;

    @Before
    public void setUp() throws Exception {
        DriverFactory.setupDriver();

    }

    public static WebDriver getDriver() {
        return DriverFactory.getWebDriver ();
    }

    @After
    public void afterScenario(){ DriverFactory.closeDriver (); }
}
