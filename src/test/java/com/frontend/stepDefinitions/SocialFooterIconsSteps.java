package com.frontend.stepDefinitions;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import pageobjects.SocialFooterIconsPage;

import static junit.framework.TestCase.assertEquals;

public class SocialFooterIconsSteps {
    SocialFooterIconsPage socialFooterIconsPage = new SocialFooterIconsPage ();

    @Given("^User clicks on Twitter icon button from social footer section$")
    public void userClicksOnTwitterIconButtonFromSocialFooterSection () {
        socialFooterIconsPage.userClicksOnTwitterImage ();
    }

    @Then("^User is redirected to Fedex twitter page$")
    public void userIsRedirectedToFedexTwitterPage () {
        assertEquals("https://twitter.com/FedExEurope",  socialFooterIconsPage.getTwitterPageTitleSelector());
    }
}
