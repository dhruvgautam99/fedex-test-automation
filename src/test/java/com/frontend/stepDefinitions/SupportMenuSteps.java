package com.frontend.stepDefinitions;

import cucumber.api.java.en.And;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import org.junit.Assert;
import pageobjects.SupportMenuPage;

public class SupportMenuSteps {
    SupportMenuPage supportMenuPage = new SupportMenuPage ();

    @Given("User clicks on Support dropdown menu")
    public void userClicksOnSupportDropdownMenu () {
        supportMenuPage.userClicksOnSupportDropdownButton();
    }

    @And("User verifies New Customer Center sub menu")
    public void userVerifiesNewCustomerCenterSubMenu () {
        Assert.assertTrue (supportMenuPage.getNewCustomerCenterSelector ().isDisplayed ());
    }

    @And("User verifies Custom Clearance sub menu")
    public void userVerifiesCustomerClearanceSubMenu () {
        Assert.assertTrue (supportMenuPage.getCustomsClearanceSelector ().isDisplayed ());
    }

    @And("User verifies Claims sub menu")
    public void userVerifiesClaimsSubMenu () {
        Assert.assertTrue (supportMenuPage.getClaimsSelector ().isDisplayed ());
    }

    @And("User verifies FAQs sub menu")
    public void userVerifiesFAQsSubMenu () {
        Assert.assertTrue (supportMenuPage.getFAQSelector ().isDisplayed ());
    }

    @And("User clicks on Contact Us in Support sub menu")
    public void userClicksOnContactUs () {
        supportMenuPage.userClicksOnContactUsButton ();
    }

    @And("User enter message for Support \"([^\"]*)\"$")
    public void userEnterMessageForSupport ( String message ) {
        supportMenuPage.userEnterMessageForSupport (message);
    }

    @Then("User submit message for Support")
    public void userSubmitMessageForSupport () {
        supportMenuPage.userSubmitMessageForSupport ();
    }
}
